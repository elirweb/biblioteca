﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteca.Core.Domain.Helper.Interfaces
{
    public interface IRepository<T> where T: class
    {
        IEnumerable<T> Parameter([Optional]string param,string sql, DbConnection cn);
        IEnumerable<T> FindAll(string sql, DbConnection cn);

    }
}
