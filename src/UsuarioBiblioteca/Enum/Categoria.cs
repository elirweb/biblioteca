﻿using System.ComponentModel;

namespace UsuarioBiblioteca.Domain.Enum
{
    public enum Categoria
    {
        [Description("Informatica")]
        Informatica,
        Portugues,
        Ciencias,
        Matematica,
        Geografia,
        Ingles
    }

   
}
